namePKG() {
createDIR
fileName=$(osascript <<AppleScript

display dialog "Please name the package using this convention:

ImageConfig_DOCK_MMDDYYYY

ex: 
Photography_DOCK_01022018" default answer "" with title "Extract Printers to JSS" with icon caution

set pkgName to text returned of result

do shell script "pkgbuild --root /Users/admin/Desktop/Dock/ROOT --identifier edu.csn.macteam /Users/admin/Desktop/tmp/" & pkgName & ".pkg" with administrator privileges

AppleScript
)

cp /Users/admin/Desktop/tmp/* /Volumes/CasperShare/Packages/
cp /Users/admin/Desktop/tmp/* /Volumes/CasperShare-1/Packages/
rm -rf /Users/admin/Desktop/Dock/
rm -rf /Users/admin/Desktop/tmp/

}

createDIR() {

mkdir /Users/admin/Desktop/tmp/
mkdir /Users/admin/Desktop/Dock/
mkdir /Users/admin/Desktop/Dock/ROOT
mkdir /Users/admin/Desktop/Dock/ROOT/System
mkdir /Users/admin/Desktop/Dock/ROOT/System/Library/
mkdir /Users/admin/Desktop/Dock/ROOT/System/Library/User\ Template
mkdir /Users/admin/Desktop/Dock/ROOT/System/Library/User\ Template/English.lproj
mkdir /Users/admin/Desktop/Dock/ROOT/System/Library/User\ Template/English.lproj/Library
mkdir /Users/admin/Desktop/Dock/ROOT/System/Library/User\ Template/English.lproj/Library/Preferences

cp /Users/admin/Library/Preferences/com.apple.dock.plist /Users/admin/Desktop/Dock/ROOT/System/Library/User\ Template/English.lproj/Library/Preferences/


chown -R root:wheel /Users/admin/Desktop/Dock/


chmod 755 /Users/admin/Desktop/Dock/ROOT/System/
chmod 755 /Users/admin/Desktop/Dock/ROOT/System/Library/
chmod 700 /Users/admin/Desktop/Dock/ROOT/System/Library/User\ Template/
chmod 755 /Users/admin/Desktop/Dock/ROOT/System/Library/User\ Template/English.lproj/
chmod 700 /Users/admin/Desktop/Dock/ROOT/System/Library/User\ Template/English.lproj/Library/
chmod 700 /Users/admin/Desktop/Dock/ROOT/System/Library/User\ Template/English.lproj/Library/Preferences/
chmod 644 /Users/admin/Desktop/Dock/ROOT/System/Library/User\ Template/English.lproj/Library/Preferences/com.apple.dock.plist 

}

ejectCasperShare() {
	ejectShare=$(osascript <<AppleScript
	tell application "Finder"
    eject "CasperShare"
end tell

AppleScript
)
}

namePKG
ejectCasperShare