DC="cydccsn02.csn.edu"	

# Standard parameter variables
computerid=`/usr/sbin/scutil --get LocalHostName`
domain="csn.edu"			        # fully qualified DNS name of Active Directory Domain
ldn="admin"
udn=""				    # username of a privileged domain user
password=""			    # password of a privileged domain user       	
ou="OU=MACs,OU=Workstations,DC=csn,DC=edu"		# Distinguished name of container for the computer

# Advanced options 
alldomains="enable"		    # 'enable' or 'disable' automatic multi-domain authentication
localhome="enable"		    # 'enable' or 'disable' force home directory to local drive
protocol="smb"			    # 'afp' or 'smb' change how home is mounted from server
mobile="disable"			    # 'enable' or 'disable' mobile account support for offline logon
mobileconfirm="disable"	    # 'enable' or 'disable' warn the user that a mobile account will be created
useuncpath="disable"		# 'enable' or 'disable' use AD SMBHome attribute to determine the home dir
user_shell="/bin/bash"		# e.g., /bin/bash or "none"

adminpassword="@rr0g@ntM@dm@n!"
adusername="MacEnroll"
adpassword="Dmx010386!"
 

# If the workstation is already bound to AD, it will report it to the JSS

 check4AD=`/usr/bin/dscl localhost -list . | grep "Active Directory"`
 if [ "${check4AD}" = "Active Directory" ]; then
 
 	  echo "The system has already been bound to $domain, exiting now."
 	  echo "<result>Active</result>"
      exit 0       
 else
 	  echo "Attempting to bind to $domain. . ."
	  
	        
      dsconfigad -f -a $computerid -domain $domain -lp $adminpassword -u $adusername -p $adpassword -mobile $mobile -mobileconfirm $mobileconfirm -passinterval 0 -preferred $DC -ou $ou
 
 	  sleep 10
 	  
 	  dsconfigad --show
 
	  
	  nameCounter=`scutil --get ComputerName`

		if [ "${#nameCounter}" -gt 15 ]; then
		echo "C.Error"
		echo "<result>C.Error</result>"
	else
		echo "Character check verified. . ."
 	    echo "<result>Bound</result>"
	fi
 fi
 
# If using a preferred DC, set it as the time server to avoid time syncing issues
systemsetup -setnetworktimeserver $DC




exit 0